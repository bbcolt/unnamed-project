import vsWallSource from './shaders/vsWall';
import vsFloorCeilSource from './shaders/vsFloorCeil';
import fsWallSource from './shaders/fsWall';
import fsFloorCeilSource from './shaders/fsFloorCeil';
import fsSelectWallSource from './shaders/fsSelectWall';
import fsSelectFloorCeilSource from './shaders/fsSelectFloorCeil';

import {mat4, vec3} from 'gl-matrix';
import Camera from './camera';
//import imgFile from './sonic.png';
import {ProgramInfo, setUniform, loadTextureAtlas, createShader, createProgram, resizeCanvasToDisplaySize, setupBuffer, setupAttributes, createOffscreenFramebuffer} from './glHelpers';
import {sectorsToRenderData} from './sector';
import {getTextureAtlasDim} from './texture';
import {SurfaceType} from './sector';

class Renderer {
  constructor(gl, camera, game) {
    this.game = game;
    this.sectors = game.sectors;

    // X
    //this.offscreenFramebuffer = createOffscreenFramebuffer(gl);

    gl.canvas.addEventListener('click', e => {
      console.log('clicked the canvas');
    });

    this.frameCount = 0;
    this.selected = 0;

    this.gl = gl;
    this.camera = camera;

    this.projectionMatrix = mat4.create();
    const aspect = this.gl.canvas.clientWidth/this.gl.canvas.clientHeight;
    const zNear = 0.1;
    const zFar = 10000;
    mat4.perspective(this.projectionMatrix, Math.PI/4, aspect, zNear, zFar);
    this.matrix = mat4.create();

    //resizeCanvasToDisplaySize(this.gl.canvas);
    this.gl.viewport(0, 0, this.gl.canvas.width, this.gl.canvas.height);

    const vsWall = createShader(this.gl, this.gl.VERTEX_SHADER, vsWallSource);
    const fsWall = createShader(this.gl, this.gl.FRAGMENT_SHADER, fsWallSource);
    const vsFloorCeil = createShader(this.gl, this.gl.VERTEX_SHADER, vsFloorCeilSource);
    const fsFloorCeil = createShader(this.gl, this.gl.FRAGMENT_SHADER, fsFloorCeilSource);
    const glWallProg = createProgram(this.gl, vsWall, fsWall);
    const glFloorCeilProg = createProgram(this.gl, vsFloorCeil, fsFloorCeil);

    this.wallProg = new ProgramInfo(this.gl, glWallProg);
    this.wallProg.addAttrib('a_position', 3, gl.FLOAT, 28, 0);
    this.wallProg.addAttrib('a_texcoord', 2, gl.FLOAT, 28, 12);
    this.wallProg.addAttrib('a_wallNum', 1, gl.FLOAT, 28, 20);
    this.wallProg.addAttrib('a_selected', 1, gl.FLOAT, 28, 24);
    this.wallProg.addUniform('u_matrix', gl.FLOAT_MAT4);
    this.wallProg.addUniform('u_selected', gl.INT);
    this.wallProg.addUniform('u_time', gl.FLOAT);
    this.wallProg.addUniform('u_mid', gl.FLOAT_VEC2);
    this.wallProg.addUniform('u_sectorNum', gl.FLOAT);

    this.floorCeilProg = new ProgramInfo(this.gl, glFloorCeilProg);
    this.floorCeilProg.addAttrib('a_position', 2, gl.FLOAT, 12, 0);
    this.floorCeilProg.addAttrib('a_selected', 1, gl.FLOAT, 12, 8);
    this.floorCeilProg.addUniform('u_matrix', gl.FLOAT_MAT4);
    this.floorCeilProg.addUniform('u_time', gl.FLOAT);
    this.floorCeilProg.addUniform('u_mid', gl.FLOAT_VEC2);
    this.floorCeilProg.addUniform('u_sectorNum', gl.FLOAT);
    this.floorCeilProg.addUniform('u_floorHeight', gl.FLOAT);
    this.floorCeilProg.addUniform('u_ceilHeight', gl.FLOAT);
    this.floorCeilProg.addUniform('u_selected', gl.INT);
    this.floorCeilProg.addUniform('u_ceilSubTexOffset', gl.FLOAT_VEC2);
    this.floorCeilProg.addUniform('u_floorSubTexOffset', gl.FLOAT_VEC2);
    this.floorCeilProg.addUniform('u_texAtlasDim', gl.INT);

    // Setup VAOs
    this.wallVAO = this.gl.createVertexArray();
    this.gl.bindVertexArray(this.wallVAO);
    this.wallProg.setupBuffer();

    this.floorCeilVAO = this.gl.createVertexArray();
    this.gl.bindVertexArray(this.floorCeilVAO);
    this.floorCeilProg.setupBuffer();

    this.renderData = {wallAttribs: [], floorCeilAttribs: [], sectors: []};
  }

  getPixelUnderReticle() {
    const pixel = new Uint8Array(4);
    this.gl.readPixels(Math.floor(this.gl.canvas.width/2),
                       Math.floor(this.gl.canvas.height/2),
                       1,
                       1,
                       this.gl.RGBA,
                       this.gl.UNSIGNED_BYTE,
                       pixel);
    return pixel;
  }

  updateViewProjectionMatrix() {
    mat4.mul(this.matrix, this.projectionMatrix, this.camera.viewMatrix);
  }

  updateVAO(renderData) {
    this.gl.bindVertexArray(this.wallVAO);
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.wallProg.buffer);
    this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(renderData.wallAttribs),
                        this.gl.STATIC_DRAW);

    this.gl.bindVertexArray(this.floorCeilVAO);
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.floorCeilProg.buffer);
    this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(renderData.floorCeilAttribs),
                        this.gl.STATIC_DRAW);
  }

  render() {
    if (this.sectors.some(sector => sector.dirty)) {
      this.renderData = sectorsToRenderData(this.sectors);
      this.updateVAO(this.renderData);
      this.sectors.forEach(sector => sector.dirty = false);
    }

    // don't need to call these two every frame
    this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);    
    this.gl.viewport(0, 0, this.gl.canvas.width, this.gl.canvas.height);
    //

    const focusedSurface = this.game.focusedSurface;

    this.gl.clearColor(1, 0.5, 0.5, 1);
    this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
    this.gl.enable(this.gl.DEPTH_TEST);
    //this.camera.updateViewMatrix();
    this.updateViewProjectionMatrix();
    const wallAttribs = this.renderData.wallAttribs;
    const floorCeilAttribs = this.renderData.floorCeilAttribs;
    for (let i = 0; i < this.renderData.sectors.length; i++) {
      this.gl.bindVertexArray(this.wallVAO); 
      this.gl.useProgram(this.wallProg.program);
      this.wallProg.setUniform('u_matrix', this.matrix);

      this.selected = (i == focusedSurface.sectorNum &&
                       focusedSurface.type == SurfaceType.WALL ||
                       focusedSurface.type == SurfaceType.LOWER_WALL ||
                       focusedSurface.type == SurfaceType.UPPER_WALL) ? focusedSurface.wallNum : -1;

      this.wallProg.setUniform('u_selected', this.selected);
      this.wallProg.setUniform('u_time', this.frameCount);
      this.wallProg.setUniform('u_mid', [this.gl.canvas.width/2, this.gl.canvas.height/2]);
      this.wallProg.setUniform('u_sectorNum', i);
      this.gl.drawArrays(
        this.gl.TRIANGLES,
        this.renderData.sectors[i].wallOffset,
        this.renderData.sectors[i].wallCount
      );

      this.gl.bindVertexArray(this.floorCeilVAO);
      this.gl.useProgram(this.floorCeilProg.program);
      this.floorCeilProg.setUniform('u_matrix', this.matrix);
      this.floorCeilProg.setUniform('u_floorHeight', this.renderData.sectors[i].floorHeight);
      this.floorCeilProg.setUniform('u_ceilHeight', this.renderData.sectors[i].ceilHeight);
      this.floorCeilProg.setUniform('u_time', this.frameCount);
      this.floorCeilProg.setUniform('u_ceilSubTexOffset', this.renderData.sectors[i].ceilSubtexOffset);
      this.floorCeilProg.setUniform('u_floorSubTexOffset', this.renderData.sectors[i].floorSubtexOffset);
      this.floorCeilProg.setUniform('u_mid', [this.gl.canvas.width/2, this.gl.canvas.height/2]);
      this.floorCeilProg.setUniform('u_sectorNum', i);

      if (i == focusedSurface.sectorNum && focusedSurface.type == SurfaceType.FLOOR)
        this.selected = 0
      else if (i == focusedSurface.sectorNum && focusedSurface.type == SurfaceType.CEIL)
        this.selected = 1
      else
        this.selected = -1;

      this.floorCeilProg.setUniform('u_selected', this.selected);
      // Can I do this once?
      this.floorCeilProg.setUniform('u_texAtlasDim', getTextureAtlasDim());

      this.gl.drawArraysInstanced(
        this.gl.TRIANGLES,
        this.renderData.sectors[i].floorCeilOffset,
        this.renderData.sectors[i].floorCeilCount,
        2 // Draw 2 instances of the base floor/ceil geometry. One for the floor, and one
          // for the ceiling 
      );
    }

    this.frameCount++;
  }
}

export {Renderer};
