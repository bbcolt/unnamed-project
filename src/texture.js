import {updateTexture, setupTexture} from './glHelpers';
import imgFile from './uvtexture2.png';

const textureAtlasDim = 4;
let selectedTexNum = 0;

export function getSelectedTexNum() {
  return selectedTexNum;
}

export function getTextureAtlasDim() {
  return textureAtlasDim;
}

export function getSubtexOffset(texNum) {
  const subtextureFract = 1 / textureAtlasDim;
  const x = (subtextureFract * texNum) % 1;
  const y = Math.floor(subtextureFract * texNum) * subtextureFract;
  return [x, y];
}

export function onImageUpload(gl, ctx, textureObj) {
  ctx.canvas.width = 512;
  ctx.canvas.height = 512;
  const texturePickerContainer = document.querySelector('#container-texture-picker');
  let numTextures = 1;
  ctx.fillStyle = 'white';
  ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  /*
  ctx.fillStyle = '#f700d6';
  ctx.fillRect(0, 0, ctx.canvas.width/textureAtlasDim, ctx.canvas.height/textureAtlasDim);
  ctx.fillStyle = 'black';
  ctx.fillRect(0, 0, ctx.canvas.width/(textureAtlasDim * 2), ctx.canvas.height/(textureAtlasDim * 2));
  ctx.fillRect(ctx.canvas.width/(textureAtlasDim * 2), ctx.canvas.height/(textureAtlasDim * 2), ctx.canvas.width/(textureAtlasDim * 2), ctx.canvas.height/(textureAtlasDim * 2));
  */
  const img = new Image();
  img.src = imgFile;
  img.onload = () => {
    ctx.drawImage(img, 0, 0, ctx.canvas.width/textureAtlasDim, ctx.canvas.height/textureAtlasDim);
    updateTexture(gl, textureObj, ctx.canvas);
  }

  const bufferCtx = document.createElement('canvas').getContext('2d');
  bufferCtx.canvas.width = ctx.canvas.width/textureAtlasDim;
  bufferCtx.canvas.height = ctx.canvas.height/textureAtlasDim;

  let prevTarget = null;

  return function(e) {
    for (const file of this.files) {
      const img = new Image();
      const reader = new FileReader();
      reader.onload = function (re) {
        img.onload = function() {
          ctx.drawImage(img,
                        0, 0,
                        this.width, this.height,
                        (numTextures % textureAtlasDim) * ctx.canvas.width/textureAtlasDim, Math.floor(numTextures / textureAtlasDim) * ctx.canvas.height / textureAtlasDim,
                        ctx.canvas.width/textureAtlasDim, ctx.canvas.height/textureAtlasDim);
          bufferCtx.drawImage(ctx.canvas,
                              (numTextures % textureAtlasDim) * ctx.canvas.width/textureAtlasDim, Math.floor(numTextures/textureAtlasDim) * ctx.canvas.height / textureAtlasDim,
                              ctx.canvas.width/textureAtlasDim, ctx.canvas.height/textureAtlasDim,
                              0, 0,
                              bufferCtx.canvas.width, bufferCtx.canvas.height);
          const imgEl = new Image();
          imgEl.src = bufferCtx.canvas.toDataURL();
          imgEl.texNum = numTextures;
          numTextures++;
          imgEl.addEventListener('click', function(e) {
            selectedTexNum = e.target.texNum;
            if (prevTarget)
              prevTarget.style.border = '';
            e.target.style.border = 'thin solid yellow';
            prevTarget = e.target;
          });
          texturePickerContainer.appendChild(imgEl);
          updateTexture(gl, textureObj, ctx.canvas);
        }
        img.src = re.target.result;
      }
      reader.readAsDataURL(file);
    }
  }
}

