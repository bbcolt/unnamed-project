// TODO: deregister command

export const InputType = Object.freeze(
  {
    MOUSE_MOVE: 'MouseMove',
    MOUSE_LEFT: '0',
    MOUSE_MIDDLE: '1',
    MOUSE_RIGHT: '2',
    MOUSE_SCROLL: 'MouseScroll',
    KEY_A: 'KeyA',
    KEY_B: 'KeyB',
    KEY_C: 'KeyC',
    KEY_D: 'KeyD',
    KEY_E: 'KeyE',
    KEY_F: 'KeyF',
    KEY_G: 'KeyG',
    KEY_H: 'KeyH',
    KEY_I: 'KeyI',
    KEY_J: 'KeyJ',
    KEY_K: 'KeyK',
    KEY_L: 'KeyL',
    KEY_M: 'KeyM',
    KEY_N: 'KeyN',
    KEY_O: 'KeyO',
    KEY_P: 'KeyP',
    KEY_Q: 'KeyQ',
    KEY_R: 'KeyR',
    KEY_S: 'KeyS',
    KEY_T: 'KeyT',
    KEY_U: 'KeyU',
    KEY_V: 'KeyV',
    KEY_W: 'KeyW',
    KEY_X: 'KeyX',
    KEY_Y: 'KeyY',
    KEY_Z: 'KeyZ',
    KEY_SPACE: 'Space',
    KEY_ARROWLEFT: 'ArrowLeft',
    KEY_ARROWRIGHT: 'ArrowRight',
    KEY_ARROWUP: 'ArrowUp',
    KEY_ARROWDOWN: 'ArrowDown',
    KEY_BACKSPACE: 'Backspace',
    KEY_SHIFT: 'Shift',  // Mod key - Don't allow registering a command with this key
    KEY_ALT: 'Alt',      // Mod key - Don't allow registering a command with this key
    KEY_CONTROL: 'Ctrl'  // Mod key - Don't allow registering a command with this key
  }
);

export const MOD_KEY_1 = InputType.KEY_SHIFT;
export const MOD_KEY_2 = InputType.KEY_ALT;
export const MOD_KEY_3 = InputType.KEY_CONTROL;

export class InputManager {
  constructor() {
    this.triggerToCommand = new Map();
    this.inputStates = {};

    window.addEventListener('keydown',   ev => { /*ev.preventDefault();*/ this.updateKeyInput(ev); });
    window.addEventListener('keyup',     ev => { /*ev.preventDefault();*/ this.updateKeyInput(ev); });
    window.addEventListener('mousemove', ev => this.updateMouseMoveInput(ev));
    window.addEventListener('mousedown', ev => this.updateMouseClickInput(ev));
    window.addEventListener('mouseup',   ev => this.updateMouseClickInput(ev));
    window.addEventListener('wheel',     ev => this.updateMouseScrollInput(ev));
  }

  updateKeyInput(ev) { this.inputStates[ev.code] = {ev, dirty: true}; }

  updateMouseClickInput(ev) { this.inputStates[ev.button] = {ev, dirty: true}; }

  updateMouseMoveInput(ev) { this.inputStates[InputType.MOUSE_MOVE] = {ev, dirty: true}; }

  updateMouseScrollInput(ev) { this.inputStates[InputType.MOUSE_SCROLL] = {ev, dirty: true}; }

  registerCommand(command, { input,
                             onRelease = false,
                             target,
                             pointerLockOnly = false,
                             argMixin = _ => null,
                             repeat = false }) {
    let eventType;
    if (input == InputType.MOUSE_MOVE)
      eventType = 'mousemove';
    else if (input == InputType.MOUSE_SCROLL)
      eventType = 'wheel';
    else if (input == InputType.MOUSE_LEFT  ||
             input == InputType.MOUSE_RIGHT ||
             input == InputType.MOUSE_MIDDLE) {
      eventType = `mouse${onRelease ? 'up' : 'down'}`;
    } else
      eventType = `key${onRelease ? 'up' : 'down'}`;

    const trigger = {
      eventType,
      input,
      target,
      pointerLockOnly,
      argMixin,
      repeat
    };
    this.triggerToCommand.set(trigger, command);
  }

  processInput() {
    for (const [trigger, command] of this.triggerToCommand.entries()) {
      const inputState = this.inputStates[trigger.input];
      if (!inputState ||
          (trigger.pointerLockOnly && !document.pointerLockElement) ||
          !inputState.dirty ||
          inputState.ev.type != trigger.eventType ||
          (trigger.target && trigger.target != inputState.ev.target) )
        continue;
      command.execute(trigger.argMixin(inputState.ev));
    }
    for (const trigger of this.triggerToCommand.keys()) {
      const inputState = this.inputStates[trigger.input];
      if (inputState && !trigger.repeat)
        inputState.dirty = false;
    }
  }
}

