
export const wraparoundArrayProxy = arr =>
  new Proxy(arr, {
    get(target, prop) {
      if (typeof prop == 'string' && !isNaN(prop))
        prop = ((parseInt(prop, 10) % target.length) + target.length) % target.length;
      return target[prop];
    }
  });
