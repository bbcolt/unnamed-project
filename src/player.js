import {vec3} from 'gl-matrix';
import Camera from './camera';

const componentwiseVectorProxyHandler = {
  get: function(target, prop) {
    if      (prop === 'x') return target[0];
    else if (prop === 'y') return target[1];
    else if (prop === 'z') return target[2];
    else                   return Reflect.get(...arguments);
  },

  set: function(target, prop, value) {
    if      (prop === 'x') target[0] = value;
    else if (prop === 'y') target[1] = value;
    else if (prop === 'z') target[2] = value;
    else                   target[prop] = value;

    return true;
  }
}

export class Player {
  constructor(pos = vec3.create(), angle = 0) {
    this.pos = new Proxy(pos, componentwiseVectorProxyHandler);
    this.angle = angle;
    this.pitch = 0
    this.camera = new Camera(this);
    this.velx = 0;
    this.vecz = 0;
    this.speed = 5;
    this.yawSpeed = 0.05;
    this.pitchSpeed = 0.05;

    this.curSector = null;
  }

  moveForward(dir) {
    this.x += Math.cos(this.angle) * dir * this.speed;
    this.z += Math.sin(this.angle) * dir * this.speed;
  }

  moveRight(dir) {
    this.x += -Math.sin(this.angle) * dir * this.speed;
    this.z +=  Math.cos(this.angle) * dir * this.speed;
  }

  moveUp(dir) {
    this.y += 5 * dir;
  }

  rotate(x, z) {
    this.angle += this.yawSpeed * -x;
    this.pitch += this.yawSpeed *  z;
  }

  get x() {
    return this.pos.x;
  }
  set x(x) {
    this.pos.x = x;
  }

  get y() {
    return this.pos.y
  }
  set y(y) {
    this.pos.y = y;
  }

  get z() {
    return this.pos.z;
  }
  set z(z) {
    this.pos.z = z;
  }

  // Should this be normalized ?
  get front() {
    return vec3.fromValues( Math.cos(this.angle) * Math.cos(this.pitch),
                            Math.sin(this.pitch),
                            Math.sin(this.angle) * Math.cos(this.pitch));
  }
}
