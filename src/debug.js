import {SurfaceType} from './sector';

export function getFocusedSurfaceDebugString(lookSurface) {
  let surfaceTypeStr;
  switch (lookSurface.type) {
    case SurfaceType.WALL:
      surfaceTypeStr = 'wall';
      break;
    case SurfaceType.LOWER_WALL:
      surfaceTypeStr = 'lower wall';
      break;
    case SurfaceType.UPPER_WALL:
      surfaceTypeStr = 'upper wall';
      break;
    case SurfaceType.FLOOR:
      surfaceTypeStr = 'floor';
      break;
    case SurfaceType.CEIL:
      surfaceTypeStr = 'ceil';
      break;
    default:
      surfaceTypeStr = 'unknown';
  }
  return `sector ${lookSurface.sectorNum}, ${surfaceTypeStr}
      ${lookSurface.type == SurfaceType.WALL ||
        lookSurface.type == SurfaceType.LOWER_WALL ||
        lookSurface.type == SurfaceType.UPPER_WALL ? ' ' + lookSurface.wallNum : ''}`;
}
