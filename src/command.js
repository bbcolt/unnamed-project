import {getSelectedTexNum} from './texture';
import {MOD_KEY_1, MOD_KEY_2, MOD_KEY_3} from './input';

// TODO: move these
export function argMixin(arg) {
  return _ => arg;
}

export function mouseMovementXZOffsetXZArgMixin(targetBoundingRect) {
  return ev => ({ x: ev.clientX - targetBoundingRect.left,
                  z: ev.clientY - targetBoundingRect.top,
                  deltaX: ev.movementX,
                  deltaZ: ev.movementY });
}

export function mouseMovementXZArgMixin(xScale = 1, zScale = 1) {
  return ev => ({x: ev.movementX * xScale, z: ev.movementY * zScale});
}

export function scrollDeltaYArgMixin(scale = 1) {
  return ev => ({dir: (ev.deltaY > 0 ? -1 : 1) * scale});
}

export function modKeysArgMixin() {
  return ev => {
    const modKeys = [];
    if (ev[`${MOD_KEY_1.toLowerCase()}Key`])
      modKeys.push(MOD_KEY_1);
    if (ev[`${MOD_KEY_3.toLowerCase()}Key`])
      modKeys.push(MOD_KEY_3);
    return {modKeys};
  };
}

export class DebugCommand {
  constructor(sb) {
    this.sb = sb;
  }
  execute(arg) {
    //console.log(this.sb.sectors);
    console.log('arg.modKey', arg.modKey);
  }
}

export class MoveCursorCommand {
  constructor(sb) {
    this.sb = sb;
  }
  execute(arg) {
    this.sb.moveCursor(arg);
  }
}

export class GrabCommand {
  constructor(sb, grab) {
    this.sb = sb;
    this.grab = grab;
  }
  execute(arg) {
    if (this.grab)
      this.sb.grab();
    else
      this.sb.release();
  }
}

export class SetIsPanningCommand {
  constructor(sb, isPanning) {
    this.sb = sb;
    this.isPanning = isPanning;
  }
  execute(arg) {
    this.sb.isPanning = this.isPanning;
  }
}

export class RequestPointerLockCommand {
  constructor(domEl) {
    this.domEl = domEl;
  }
  execute(arg) {
    this.domEl.requestPointerLock();
  }
}

export class DropNodeCommand {
  constructor(sectorBuilder) {
    this.sb = sectorBuilder;
  }
  execute(arg) {
    this.sb.dropNode();
  }
}

export class UnselectAllSurfacesCommand {
  constructor(game) {
    this.game = game;
  }
  execute(arg) {
    this.game.unselectAllSurfaces();
  }
}

export class ToggleSurfaceSelectionCommand {
  constructor(game) {
    this.game = game;
  }
  execute(arg) {
    // selecting all adjacent walls takes priority
    const selectionType = (arg.modKeys.includes(MOD_KEY_3)) ? 'adjacent'
                        : (arg.modKeys.includes(MOD_KEY_1)) ? 'multi'
                        :                                     'single';

    this.game.toggleSurfaceSelection(this.game.focusedSurface, selectionType);
  }
}

export class SetTextureCommand {
  constructor(game) {
    this.game = game;
  }
  execute(arg) {
    this.game.applyTexture();
  }
}

export class RaiseFloorCeilCommand {
  constructor(game) {
    this.game = game;
  }
  execute(arg) {
    this.game.raiseFloorCeil(arg.dir);
  }
}

export class MoveForwardCommand {
  constructor(entity) {
    this.entity = entity;
  }
  execute(arg) {
    this.entity.moveForward(arg.dir);
  }
}

export class MoveRightCommand {
  constructor(entity) {
    this.entity = entity;
  }
  execute(arg) {
    this.entity.moveRight(arg.dir);
  }
}

export class MoveUpCommand {
  constructor(entity) {
    this.entity = entity;
  }
  execute(arg) {
    this.entity.moveUp(arg.dir);
  }
}

export class RotateCommand {
  constructor(entity) {
    this.entity = entity;
  }
  execute(arg) {

    this.entity.rotate(arg.x, arg.z);
  }
}

