import {mat4, vec3} from 'gl-matrix';

export default class Camera {
  //constructor(position = [0, 0, 0], yaw, /*front = [1, 0, 0]*/) {
  constructor(entity) {
    this.entity = entity;
    this.eye = entity.pos;
    //this.front = front;
    this.worldUp = vec3.fromValues(0, 1, 0);

    //this.yaw = 0;
    //this.pitch = 0;
  }

  get yaw() {
    return this.entity.angle;
  }

  get pitch() {
    return this.entity.pitch;
  }

  get front() {
    return this.entity.front;
  }

  get viewMatrix() {
    return mat4.lookAt(mat4.create(),           // out
                       this.eye,                // position
                       vec3.add(vec3.create(),  // forward
                                this.eye,
                                this.front),
                       this.worldUp);                // world up
  }

  /*
  get right() {
    return vec3.normalize(vec3.create(),
                          vec3.cross(vec3.create(),
                                     this.front,
                                     this.worldUp));
  }

  set right(v) {
    
  }


  moveForward(dir) {
    console.log(this.front);
    let forward = vec3.create();
    vec3.cross(forward,
               this.right,
               this.worldUp);
    vec3.scale(forward,
               forward,
               dir * -1);
    vec3.add(this.eye,
             this.eye,
             forward);
  }

  moveRight(dir) {
    vec3.add(this.eye,
             this.eye,
             vec3.scale(vec3.create(), 
                        this.right,
                        dir * 1));
  }

  turnClockwise(angle) {
    this.yaw -= angle;
    const front = [
      Math.cos(this.yaw) * Math.cos(this.pitch),
      Math.sin(this.pitch),
      Math.sin(this.yaw) * Math.cos(this.pitch)
    ];
    vec3.normalize(this.front, front);
  }

  pitchUp(angle) {
    this.pitch += angle;
    const front = [
      Math.cos(this.yaw) * Math.cos(this.pitch),
      Math.sin(this.pitch),
      Math.sin(this.yaw) * Math.cos(this.pitch)
    ];
    vec3.normalize(this.front, front);
  }

  moveUp(dir) {
    vec3.add(this.eye,
             this.eye,
             vec3.scale(vec3.create(),
                        vec3.fromValues(0, 1, 0),
                        dir * 1));
  }
*/
}
