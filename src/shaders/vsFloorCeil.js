const vsFloorCeil = `#version 300 es
in vec2 a_position;
in float a_selected;

uniform mat4 u_matrix;
uniform float u_ceilHeight;
uniform float u_floorHeight;

out vec2 v_texcoord;
flat out int vid;
flat out int instanceID;
flat out float surface;
flat out int ss;

void main() {
  float h;
  if (gl_InstanceID == 0) {
    h = u_floorHeight;
    surface = 3.0;
  }
  else {
    h = u_ceilHeight;
    surface = 4.0;
  }
  gl_Position = u_matrix * vec4(a_position.x, h, a_position.y, 1.0);

  float scale = 100.0;
  float s = a_position.x/scale;
  float t = a_position.y/scale;
  /*
  float min1 = 0.0;
  float max1 = 50.0;
  float min2 = 0.0;
  float max2 = 100.0;
  s = min2 + (s - min1) * (max2 - min2) / (max1 - min1);
  t = min2 + (t - min1) * (max2 - min2) / (max1 - min1);
  */
  v_texcoord = vec2(s, t);
  v_texcoord = v_texcoord;
  vid = gl_VertexID;
  instanceID = gl_InstanceID;

  ss = int(a_selected);
}
`;

export default vsFloorCeil;
