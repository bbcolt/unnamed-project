const fsSelectFloorCeil = `#version 300 es
precision mediump float;

flat in int instanceID;

uniform int u_sectorIdx;

out vec4 outColor;

void main() {
  int bits = 0x0;
  bits |= 0x80;
  bits |= (instanceID) << (instanceID * 6);
  
  outColor = vec4(float(bits)/255.0, 0.0, 0.0, float(u_sectorIdx)/255.0);
}
`;

export default fsSelectFloorCeil;
