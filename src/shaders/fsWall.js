const fsWall = `#version 300 es
precision mediump float;

in vec2 v_texcoord;
flat in int focused;
flat in float wallNum;
flat in int ss;

uniform sampler2D sampler;
uniform float u_time; 
uniform vec2 u_mid;
uniform float u_sectorNum;

out vec4 outColor;

void main() {
  outColor = texture(sampler, v_texcoord);
  //outColor *= 1.0/(gl_FragCoord.w * 0.001); //(0.2 * gl_FragCoord.z - gl_DepthRange.near - gl_DepthRange.far);

  if (focused == 1) {
    outColor.r *= 1.4;
    outColor.g *= 1.4;
    //outColor.x *= (sin(u_time * 0.05) + 3.0)/4.0;
    //outColor.y *= (sin(u_time * 0.05) + 3.0)/4.0;
    //outColor.z *= (sin(u_time * 0.05) + 4.0)/4.0;
  }

  if (ss == 1) {
    outColor.x *= (sin(u_time * 0.059) + 3.0)/4.0;
    outColor.y *= (sin(u_time * 0.059) + 3.0)/4.0;
    outColor.z *= (sin(u_time * 0.059) + 3.0)/4.0;
  }

  if (abs(gl_FragCoord.x - u_mid.x) < 1.0 &&
      abs(gl_FragCoord.y - u_mid.y) < 1.0) {
    float i;
    float fr = modf(wallNum, i);
    outColor = vec4(u_sectorNum/255.0,
                    i/255.0,
                    0.0/255.0,
                    fr/25.5);
    //outColor = vec4(u_sectorNum/255.0, (wallNum + 1.0)/255.0, 0.0, 0.0);
  }
}
`;

export default fsWall;
