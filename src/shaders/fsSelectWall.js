const fsSelectWall = `#version 300 es
precision mediump float;

in vec2 v_texcoord;
flat in int vid;

out vec4 outColor;

void main() {
  float r = float(vid);
  outColor = vec4(r/255.0, 0.4, 0.6, 1.0);
}
`;

export default fsSelectWall;
