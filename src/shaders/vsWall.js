const vsWall = `#version 300 es
in vec3 a_position;
in vec2 a_texcoord;
in float a_wallNum;
in float a_selected;

uniform mat4 u_matrix;
uniform int u_selected;

out vec2 v_texcoord;
flat out int focused;
flat out float wallNum;
flat out int ss;

void main() {
    gl_Position = u_matrix * vec4(a_position, 1);

    v_texcoord = a_texcoord;
    //if ((u_selected*6)+2 == gl_VertexID || (u_selected*6)+5 == gl_VertexID) {
    if ( abs(float(u_selected) - a_wallNum) < 0.1 ) {
      focused = 1;
    } else { 
      focused = 0;
    }

    //ss = (abs(a_s - 123.0) < 0.01) ? 1 : 0;
    ss = int(a_selected);

    wallNum = a_wallNum;
}
`;

export default vsWall;
