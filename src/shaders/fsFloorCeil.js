const fs = `#version 300 es
precision mediump float;

in vec2 v_texcoord;
flat in int vid;
flat in int instanceID;
flat in float surface;
flat in int ss;

uniform sampler2D sampler;
//uniform bool u_isSelected;
uniform float u_time; 
uniform int u_selected;
uniform int u_texAtlasDim;
uniform vec2 u_mid;
uniform float u_sectorNum;

uniform vec2 u_ceilSubTexOffset;
uniform vec2 u_floorSubTexOffset;

out vec4 outColor;

void main() {
  vec2 texSize = vec2(1.0 / float(u_texAtlasDim), 1.0 / float(u_texAtlasDim));
  vec2 offset;
  if (instanceID == 0)
    offset = u_floorSubTexOffset;
   else
    offset = u_ceilSubTexOffset;

  outColor = texture(sampler, offset + texSize * fract(v_texcoord));

  if ((instanceID == 1 && u_selected == 1) || (instanceID == 0 && u_selected == 0)) {
    outColor.r *= 1.4;
    outColor.g *= 1.4;
  }

  if ((instanceID == 0 && ss == 1) || (instanceID == 1 && ss == 2)) {
    outColor.r *= (sin(u_time * 0.059) + 4.0)/4.0;
    outColor.g *= (sin(u_time * 0.059) + 4.0)/4.0;
  }

  if (abs(gl_FragCoord.x - u_mid.x) < 1.0 &&
      abs(gl_FragCoord.y - u_mid.y) < 1.0)
      outColor = vec4(u_sectorNum/255.0, 0.0, 0.0, surface/255.0);
}
`;

export default fs;
