export class ProgramInfo {
  constructor(gl, program) {
    this.gl = gl;
    this.program = program;
    this.buffer = gl.createBuffer();
    this.attribs = {};
    this.uniforms = {};
  }

  addAttrib(varName, numComponents, type, stride, offset) {
    this.attribs[varName] = {};
    this.attribs[varName].location = this.gl.getAttribLocation(this.program, varName);
    this.attribs[varName].numComponents = numComponents;
    this.attribs[varName].type = type;
    this.attribs[varName].stride = stride;
    this.attribs[varName].offset = offset;
  }

  addUniform(varName, type) {
    this.uniforms[varName] = {};
    this.uniforms[varName].type = type;
    this.uniforms[varName].location = this.gl.getUniformLocation(this.program, varName);
  }

  setUniform(varName, val) {
    const uniform = this.uniforms[varName];
    switch (uniform.type) {
      case this.gl.FLOAT:
        this.gl.uniform1f(uniform.location, val);
        break;
      case this.gl.FLOAT_VEC2:
        this.gl.uniform2fv(uniform.location, val);
        break;
      case this.gl.FLOAT_VEC3:
        this.gl.uniform3fv(uniform.location, val);
        break;
      case this.gl.FLOAT_MAT4:
        this.gl.uniformMatrix4fv(uniform.location, false, val);
        break;
      case this.gl.BOOL:
        this.gl.uniform1i(uniform.location, val);
        break;
      case this.gl.INT:
        this.gl.uniform1i(uniform.location, val);
        break;
      default:
        console.error(`Unknown uniform type ${uniform.type}`);
    }
  }

  setupBuffer() {
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.buffer);
    for (let name in this.attribs) {
      this.gl.vertexAttribPointer(
        this.attribs[name].location,
        this.attribs[name].numComponents,
        this.attribs[name].type,
        false,
        this.attribs[name].stride,
        this.attribs[name].offset
      );
      this.gl.enableVertexAttribArray(this.attribs[name].location);
    }
  }
}

export function setupTexture(gl, textureObj) {
  gl.activeTexture(gl.TEXTURE0 + 0);
  gl.bindTexture(gl.TEXTURE_2D, textureObj);
  const pixel = new Uint8Array([0, 0, 255, 255]);
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, pixel);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
}

export function updateTexture(gl, textureObj, img) {
  gl.bindTexture(gl.TEXTURE_2D, textureObj);
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, img);
}

export function createOffscreenFramebuffer(gl) {
  const fb = gl.createFramebuffer();
  if (!fb) alert('Failed to create framebuffer object');
  gl.bindFramebuffer(gl.FRAMEBUFFER, fb);
  const db = gl.createRenderbuffer();
  if (!db) alert('Failed to create renderbuffer object');
  gl.bindRenderbuffer(gl.RENDERBUFFER, db);
  gl.renderbufferStorage(gl.RENDERBUFFER,
    gl.DEPTH_COMPONENT16,
    gl.canvas.width,
    gl.canvas.height
  );
  gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, db);
  const colorTexture = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, colorTexture);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.canvas.width, gl.canvas.height, 0,
    gl.RGBA, gl.UNSIGNED_BYTE, null);
  gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D,
    colorTexture, 0);
  if (gl.checkFramebufferStatus(gl.FRAMEBUFFER) != gl.FRAMEBUFFER_COMPLETE) {
    console.log(gl.checkFramebufferStatus(gl.FRAMEBUFFER));
    alert('This combination of attachments does not work');
  }
  return fb;
}

export function setupBuffer(gl, programInfo) {
  alert('delete this function');
  const buffer = programInfo.buffer;
  for (let [_, attrib] of Object.entries(programInfo.attribs)) {
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.vertexAttribPointer(
      attrib.location,
      attrib.numComponents,
      attrib.type,
      false,
      attrib.stride,
      attrib.offset
    );
    gl.enableVertexAttribArray(attrib.location);
  }
}

export function createShader(gl, type, source) {
  let shader = gl.createShader(type);
  gl.shaderSource(shader, source);
  gl.compileShader(shader);
  let success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
  if (success)
    return shader;

  console.error(gl.getShaderInfoLog(shader));
  gl.deleteProgram(shader);
}

export function createProgram(gl, vertexShader, fragmentShader) {
  let program = gl.createProgram();
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  gl.linkProgram(program);
  let success = gl.getProgramParameter(program, gl.LINK_STATUS);
  if (success)
    return program;
  console.error(gl.getProgramInfoLog(program));
  gl.deleteProgram(program);
  return undefined;
}

export function resizeCanvasToDisplaySize(canvas, multiplier = 1) {
  /*
  const width = canvas.clientWidth * multiplier | 0;
  const height = canvas.clientHeight * multiplier | 0;
  if (canvas.width !== width || canvas.height !== height) {
    canvas.width = width;
    canvas.height = height;
    return true;
  }
  return false;
  */
}

export function setUniform(gl, uniform, data) {
  switch(uniform.type) {
    case gl.FLOAT_MAT4:
      gl.uniformMatrix4fv(uniform.location, false, data);
      break;
    case gl.FLOAT:
      gl.uniform1f(uniform.location, data);
      break;
    case gl.BOOL:
      gl.uniform1i(uniform.location, data);
      break;
      // ...
    default:
      throw new Error(`Unknown uniform data type ${uniform.type}`);
  }
}

