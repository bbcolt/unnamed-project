import earcut from 'earcut';
import {getSubtexOffset, getTextureAtlasDim} from './texture';
import {intersect} from './raycast';
import {wraparoundArrayProxy} from './util';

export const SurfaceType = Object.freeze({'WALL': 0, 'LOWER_WALL': 1, 'UPPER_WALL': 2, 'FLOOR': 3, 'CEIL': 4});
const DEFAULT_CEIL_HEIGHT = 80;
const DEFAULT_FLOOR_HEIGHT = -80;

export function curSector(lastKnownSector, sectors, playerX, playerY) {
  if (pointInSector(playerX, playerY, lastKnownSector)) {
    return lastKnownSector;
  } else if (lastKnownSector) {
    for (const node of lastKnownSector.nodes) {
      if (node.neighboringSector != null) {
        if (pointInSector(playerX, playerY, node.neighboringSector))
          return node.neighboringSector;
      }
    }
  }
  for (const sector of sectors) {
    if (pointInSector(playerX, playerY, sector))
      return sector;
  }
  return null;
}

export function pointInSector(x, y, sector) {
  if (!sector) return false;
  const x1 = x;
  const y1 = y;
  const x2 = x + 9999;
  const y2 = y;

  let count = 0;
  for (const node of sector.nodes) {
    if (!node.next)
      return false;
    const inter = intersect(x1, y1, x2, y2, node.x, node.z, node.next.x, node.next.z);
    if (inter != null) {
      count++;
    }
  }
  return (count % 2 == 1);
}

export function createNode(x, z, sector = null) {
  // maybe combine texNum, lower/uppertexNum into one number and parse it
  return {
    x, z,
    prev: null, next: null,
    sector, neighboringSector: null,
    texNum: 0, lowerTexNum: 0, upperTexNum: 0,
    selected: 0
  };
}

export function createSector(ceilHeight = DEFAULT_CEIL_HEIGHT, floorHeight = DEFAULT_FLOOR_HEIGHT) {
  return { nodes: wraparoundArrayProxy([]),
           ceilHeight,
           floorHeight,
           dirty: false,
           ceilTexNum: 0,
           floorTexNum: 0,
           selected: 0 };
}

// X
export function addNodeToSector(node, sector) {
  if (sector.nodes.length > 0) {
    sector.nodes[sector.nodes.length - 1].next = node;
    node.prev = sector.nodes[sector.nodes.length - 1];
  }
  node.sector = sector;
  sector.nodes.push(node);
}

// X
function sectorToPortalWallVerts(sector) {
  const verts = [];
  for (let i = 0; i < sector.nodes.length; i++) {
    if (sector.nodes[i].neighboringSector == null)
      continue;

    const nextIdx = (i === sector.nodes.length - 1) ? 0 : (i + 1);
    if (sector.ceilHeight > sector.nodes[i].neighboringSector.ceilHeight) {
      const topTop = sector.ceilHeight;
      const topBottom = sector.nodes[i].neighboringSector.ceilHeight;

      verts.push(sector.nodes[i].x, topTop, sector.nodes[i].z);
      verts.push(sector.nodes[i].x, topBottom, sector.nodes[i].z);
      verts.push(sector.nodes[nextIdx].x, topTop, sector.nodes[nextIdx].z);

      verts.push(sector.nodes[nextIdx].x, topTop, sector.nodes[nextIdx].z);
      verts.push(sector.nodes[i].x, topBottom, sector.nodes[i].z);
      verts.push(sector.nodes[nextIdx].x, topBottom, sector.nodes[nextIdx].z);
    }

    if (sector.floorHeight < sector.nodes[i].neighboringSector.floorHeight) {
      const bottomTop = sector.nodes[i].neighboringSector.floorHeight;
      const bottomBottom = sector.floorHeight;

      verts.push(sector.nodes[i].x, bottomTop, sector.nodes[i].z);
      verts.push(sector.nodes[i].x, bottomBottom, sector.nodes[i].z);
      verts.push(sector.nodes[nextIdx].x, bottomTop, sector.nodes[nextIdx].z);

      verts.push(sector.nodes[nextIdx].x, bottomTop, sector.nodes[nextIdx].z);
      verts.push(sector.nodes[i].x, bottomBottom, sector.nodes[i].z);
      verts.push(sector.nodes[nextIdx].x, bottomBottom, sector.nodes[nextIdx].z);
    }
  }

  return verts;
}

// X
function sectorToWallVerts(sector) {
  const verts = [];
  const floorHeight = sector.floorHeight;
  const ceilHeight = sector.ceilHeight;
  for (let i = 0; i < sector.nodes.length; i++) {
    if (sector.nodes[i].neighboringSector != null)
      continue;
    const nextIdx = (i === sector.nodes.length - 1) ? 0 : (i + 1);
    verts.push(sector.nodes[i].x, ceilHeight, sector.nodes[i].z);
    verts.push(sector.nodes[i].x, floorHeight, sector.nodes[i].z);
    verts.push(sector.nodes[nextIdx].x, ceilHeight, sector.nodes[nextIdx].z);

    verts.push(sector.nodes[nextIdx].x, ceilHeight, sector.nodes[nextIdx].z);
    verts.push(sector.nodes[i].x, floorHeight, sector.nodes[i].z);
    verts.push(sector.nodes[nextIdx].x, floorHeight, sector.nodes[nextIdx].z);
  }
  return verts;
}

function sectorToWallAttribs(sector) {
  const attribs = [];
  const floorHeight = sector.floorHeight;
  const ceilHeight = sector.ceilHeight;
  let wallNum = 0;
  let s = -1;
  for (let i = 0; i < sector.nodes.length; i++) {
    const nextIdx = (i === sector.nodes.length - 1) ? 0 : (i + 1);
    if (sector.nodes[i].neighboringSector == null) { // full wall
      const uvs = createUVs(sector.nodes[i], 0);
      const selected = (sector.nodes[i].selected & 0b001);
      attribs.push(sector.nodes[i].x, ceilHeight, sector.nodes[i].z);
      attribs.push(uvs[0], uvs[1]);
      attribs.push(wallNum);
      attribs.push(selected);
      attribs.push(sector.nodes[i].x, floorHeight, sector.nodes[i].z);
      attribs.push(uvs[2], uvs[3]);
      attribs.push(wallNum);
      attribs.push(selected);
      attribs.push(sector.nodes[nextIdx].x, ceilHeight, sector.nodes[nextIdx].z);
      attribs.push(uvs[4], uvs[5]);
      attribs.push(wallNum);
      attribs.push(selected);

      attribs.push(sector.nodes[nextIdx].x, ceilHeight, sector.nodes[nextIdx].z);
      attribs.push(uvs[6], uvs[7]);
      attribs.push(wallNum);
      attribs.push(selected);
      attribs.push(sector.nodes[i].x, floorHeight, sector.nodes[i].z);
      attribs.push(uvs[8], uvs[9]);
      attribs.push(wallNum);
      attribs.push(selected);
      attribs.push(sector.nodes[nextIdx].x, floorHeight, sector.nodes[nextIdx].z);
      attribs.push(uvs[10], uvs[11]);
      attribs.push(wallNum);
      attribs.push(selected);
    } else { // portal
      if (sector.floorHeight < sector.nodes[i].neighboringSector.floorHeight) {
        // lower wall
        const bottomTop = sector.nodes[i].neighboringSector.floorHeight;
        const bottomBottom = floorHeight;
        const uvs = createUVs(sector.nodes[i], -1);
        const selected = (sector.nodes[i].selected & 0b010) >> 1;
        attribs.push(sector.nodes[i].x, bottomTop, sector.nodes[i].z);
        attribs.push(uvs[0], uvs[1]);
        attribs.push(wallNum + 0.1);
        attribs.push(selected);
        attribs.push(sector.nodes[i].x, bottomBottom, sector.nodes[i].z);
        attribs.push(uvs[2], uvs[3]);
        attribs.push(wallNum + 0.1);
        attribs.push(selected);
        attribs.push(sector.nodes[nextIdx].x, bottomTop, sector.nodes[nextIdx].z);
        attribs.push(uvs[4], uvs[5]);
        attribs.push(wallNum + 0.1);
        attribs.push(selected);

        attribs.push(sector.nodes[nextIdx].x, bottomTop, sector.nodes[nextIdx].z);
        attribs.push(uvs[6], uvs[7]);
        attribs.push(wallNum + 0.1);
        attribs.push(selected);
        attribs.push(sector.nodes[i].x, bottomBottom, sector.nodes[i].z);
        attribs.push(uvs[8], uvs[9]);
        attribs.push(wallNum + 0.1);
        attribs.push(selected);
        attribs.push(sector.nodes[nextIdx].x, bottomBottom, sector.nodes[nextIdx].z);
        attribs.push(uvs[10], uvs[11]);
        attribs.push(wallNum + 0.1);
        attribs.push(selected);
      }
      if (sector.ceilHeight > sector.nodes[i].neighboringSector.ceilHeight) {
        // upper wall
        const topTop = ceilHeight;
        const topBottom = sector.nodes[i].neighboringSector.ceilHeight;
        const uvs = createUVs(sector.nodes[i], 1);
        const selected = (sector.nodes[i].selected & 0b100) >> 2;
        attribs.push(sector.nodes[i].x, topTop, sector.nodes[i].z);
        attribs.push(uvs[0], uvs[1]);
        attribs.push(wallNum + 0.2);
        attribs.push(selected);
        attribs.push(sector.nodes[i].x, topBottom, sector.nodes[i].z);
        attribs.push(uvs[2], uvs[3]);
        attribs.push(wallNum + 0.2);
        attribs.push(selected);
        attribs.push(sector.nodes[nextIdx].x, topTop, sector.nodes[nextIdx].z);
        attribs.push(uvs[4], uvs[5]);
        attribs.push(wallNum + 0.2);
        attribs.push(selected);


        attribs.push(sector.nodes[nextIdx].x, topTop, sector.nodes[nextIdx].z);
        attribs.push(uvs[6], uvs[7]);
        attribs.push(wallNum + 0.2);
        attribs.push(selected);
        attribs.push(sector.nodes[i].x, topBottom, sector.nodes[i].z);
        attribs.push(uvs[8], uvs[9]);
        attribs.push(wallNum + 0.2);
        attribs.push(selected);
        attribs.push(sector.nodes[nextIdx].x, topBottom, sector.nodes[nextIdx].z);
        attribs.push(uvs[10], uvs[11]);
        attribs.push(wallNum + 0.2);
        attribs.push(selected);
      }
    }
    wallNum++;
  }
  return attribs;
}

function sectorToFloorCeilVerts(sector) {
  const points = sector.nodes.flatMap(node => [node.x, node.z]);
  /*
  const holes = [];
  for (const innerSector of sector.innerSectors) {
    holes.push(points.length/2);
    points.push(innerSector.nodes.flatMap(node => [node.x, node.z]));
  }
  const triangles = earcut(points, [holes]);
  */
  const triangles = earcut(points);
  const verts = [];
  for (let i = 0; i < triangles.length; i+=3) {
    verts.push(
      points[triangles[i  ] * 2], points[triangles[i  ] * 2 + 1], sector.selected,
      points[triangles[i+1] * 2], points[triangles[i+1] * 2 + 1], sector.selected,
      points[triangles[i+2] * 2], points[triangles[i+2] * 2 + 1], sector.selected
    );
  }

  return verts;
}

function createWallNums(sector) {
  const wallNums = [];
  for (let i = 0; i < sector.nodes.length; i++) {
    if (!sector.nodes[i].neighboringSector) {
      wallNums.push(i);
    }
  }
  return wallNums;
}

function createPortalWallNums(sector) {
  const wallNums = [];
  for (let i = 0; i < sector.nodes.length; i++) {
    if (sector.nodes[i].neighboringSector) {
      if (sector.ceilHeight > sector.nodes[i].neighboringSector.ceilHeight) {
        wallNums.push(i);
      }
      if (sector.floorHeight < sector.nodes[i].neighboringSector.floorHeight) {
        wallNums.push(i);
      }
    }
  }
  return wallNums;
}

export function sectorsToRenderData(sectors) {
  const wallAttribs = [];
  const floorCeilAttribs = []
  const sectorData = [];
  for (const sector of sectors) {
    //const wallOffset = wallAttribs.length/6;
    const wallOffset = wallAttribs.length/7;
    const floorCeilOffset = floorCeilAttribs.length/3;

    wallAttribs.push(...sectorToWallAttribs(sector));

    floorCeilAttribs.push(...sectorToFloorCeilVerts(sector));

    //const wallCount = (wallAttribs.length/6) - (wallOffset);
    const wallCount = (wallAttribs.length/7) - (wallOffset);
    const floorCeilCount = (floorCeilAttribs.length/3) - (floorCeilOffset);
    sectorData.push(
    { floorHeight: sector.floorHeight,
      ceilHeight: sector.ceilHeight,
      wallOffset,
      floorCeilOffset,
      wallCount,
      floorCeilCount,
      ceilSubtexOffset: getSubtexOffset(sector.ceilTexNum),
      floorSubtexOffset: getSubtexOffset(sector.floorTexNum)
    });
  }
  return {
    wallAttribs,
    floorCeilAttribs,
    sectors: sectorData
  };
}

// rename ?
function createUVs(node, wallType) {
  let asdf;
  if (wallType == 1) {
    asdf = 'upperTexNum';
  } else if (wallType == -1) {
    asdf = 'lowerTexNum';
  } else {
    asdf = 'texNum';
  }
  const texAtlasDim = getTextureAtlasDim();
  const subTexDim = 1/texAtlasDim;
  const x1 = (node[asdf] % texAtlasDim) * subTexDim;
  const y1 = Math.floor(node[asdf] / texAtlasDim) * subTexDim;
  const x2 = (node[asdf] % texAtlasDim) * subTexDim + subTexDim;
  const y2 = Math.floor(node[asdf] / texAtlasDim) * subTexDim + subTexDim;
  return [x1, y1,
          x1, y2,
          x2, y1,
          x2, y1,
          x1, y2,
          x2, y2];
}

