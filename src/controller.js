class Controller {
    constructor(camera) {
        this.camera = camera;
    }

    onUp() {
        this.camera.moveUp(1);
    }

    onDown() {
        this.camera.moveUp(-1);
    }

    onTurn(angle) {
        this.camera.yaw(angle);
    }

    onForward() {
        this.camera.moveForward(1);
    }

    onBack() {
        this.camera.moveForward(-1);
    }

    onLeft() {
        this.camera.moveRight(-1);
    }

    onRight() {
        this.camera.moveRight(1);
    }
}

export default Controller;
