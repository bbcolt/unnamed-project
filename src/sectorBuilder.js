// TODO: zoom grid
// TODO: This whole thing could use a rewrite
import {createNode, createSector, addNodeToSector, pointInSector} from './sector';

const can = document.querySelector('#canvas-map');
can.style.backgroundColor = 'lightblue';
const mapCanvasContainer = document.querySelector('#container-canvas-map');
can.width = mapCanvasContainer.clientWidth;
can.height = mapCanvasContainer.clientHeight;
const ctx = can.getContext('2d');
const patternCan = document.createElement('canvas');
const patternCtx = patternCan.getContext('2d');

const GRID_CELL_SIZE = 20;

const cursorDebug = document.querySelector('#cursor-coords');

class SectorBuilder {
  constructor(sectors) {
    this.openSector = createSector();
    const node = createNode(0, 0, this.openSector);
    this.openSector.nodes.push(node);
    this.grabbedNodes = [node];
    this.sectors = sectors;
    this.sectors.push(this.openSector);
    this.cursor = {x: 0, z: 0};
    this.gridOffsetX = 0;
    this.gridOffsetZ = 0;
    this.gridLineOffsetX = 0;
    this.gridLineOffsetZ = 0;
    this.isPanning = false;

    patternCan.width = GRID_CELL_SIZE;
    patternCan.height = GRID_CELL_SIZE;
  }

  moveCursor({x, z, deltaX, deltaZ}) {
    if (this.isPanning) {
      this.gridOffsetX += deltaX;
      this.gridOffsetZ += deltaZ;
    }
    this.cursor.x = Math.round((x - this.gridOffsetX)/GRID_CELL_SIZE)*GRID_CELL_SIZE + this.gridOffsetX;
    this.cursor.z = Math.round((z - this.gridOffsetZ)/GRID_CELL_SIZE)*GRID_CELL_SIZE + this.gridOffsetZ;
    cursorDebug.innerHTML = `${this.cursor.x - this.gridOffsetX}, ${this.cursor.z - this.gridOffsetZ}`;

    for (const node of this.grabbedNodes) {
      [node.x, node.z] = [this.cursor.x - this.gridOffsetX, this.cursor.z - this.gridOffsetZ];
      if (node.sector != this.openSector)
        node.sector.dirty = true;
    }
  }

  grab() {
    if (this.openSector.nodes.length > 1) {
      console.log('[grab] this.openSector.nodes.length > 1');
      return;
    }

    if (this.grabbedNodes.length > 1)
      console.error('There should only be one grabbed node here');
    this.grabbedNodes = this.coordsToNodes(this.grabbedNodes[0].x, this.grabbedNodes[0].z);
    console.log(`grabbing ${this.grabbedNodes.length} nodes`);
  }

  release() {
    if (this.openSector.nodes.length > 1)
      return;

    this.grabbedNodes = [this.openSector.nodes[0]];
  }

  coordsToNodes(x, z) {
    return this.sectors.flatMap(sector => sector.nodes)
                       .filter(node => node.x == x && node.z == z);
  }

  drawSector(sector) {
    if (sector.nodes.length < 2)
      return;

    // Draw walls
    ctx.beginPath();
    ctx.lineWidth = 2;
    for (const [i, node] of sector.nodes.entries()) {
      if (i == 0)
        ctx.moveTo(node.x + this.gridOffsetX, node.z + this.gridOffsetZ);
      else
        ctx.lineTo(node.x + this.gridOffsetX, node.z + this.gridOffsetZ);
    }
    if (sector != this.openSector) {
      ctx.strokeStyle = 'green';
      ctx.closePath();
    }
    else {
      ctx.strokeStyle = 'yellow';
    }
    ctx.stroke();

    // Draw nodes
    ctx.beginPath();
    for (const node of sector.nodes) {
      ctx.moveTo(node.x + this.gridOffsetX, node.z + this.gridOffsetZ);
      ctx.arc(node.x + this.gridOffsetX,
              node.z + this.gridOffsetZ,
              4,
              0,
              2*Math.PI);
    }
    ctx.fillStyle = 'black';
    ctx.fill();
  }

  dropNode() {
    if (this.grabbedNodes.length > 1)
      console.error('There should only be one grabbed node here');
    const node = this.grabbedNodes[0];

    // Check whether sector is being split in two
    if (this.openSector.nodes.length == 2) {
      // TODO: Handle case where chord is formed by only one wall
      const splitSector = this.sectors.find(s => pointInSector(node.x, node.z, s));
      if (splitSector) {
        console.log('this sector will split another sector in two');
        this.openSector.splitSector = splitSector;
      }
    }

    const newNode = {...node};
    if (this.closeSector(this.openSector)) {
      // sector has been closed
      this.openSector.dirty = true;
      this.openSector = createSector();
      this.openSector.nodes.push(newNode);
      newNode.sector = this.openSector;
      this.sectors.push(this.openSector);
    } else {
      // sector cannot be closed
      if (this.openSector.nodes.length == 1) {
        // starting a new sector
        const containingSector = this.sectors.find(s => pointInSector(this.openSector.nodes[0].x, this.openSector.nodes[0].z, s));
        if (containingSector) {
          // TODO: Handle creation of sectors within sectors
          console.log('this sector is a subsector');
        }
      }
      this.openSector.nodes.push(newNode);
    }
    this.grabbedNodes = [newNode];
  }

  completeOpenSector(path) {
    // Assumes the open sector is oriented clockwise
    this.openSector.nodes.push(...path.map(n => createNode(n.x, n.z)));
    for (let i = 0; i < this.openSector.nodes.length; i++) {
      this.openSector.nodes[i].next = this.openSector.nodes[i + 1];
      this.openSector.nodes[i].prev = this.openSector.nodes[i - 1];
      this.openSector.nodes[i].sector = this.openSector;

      // Mark neighboring sectors
      // Check whether a node and its next node form a wall adjacent to an existing sector
      const sharedWithCurr = this.coordsToNodes(this.openSector.nodes[i].x, this.openSector.nodes[i].z).filter(n => n.sector !== this.openSector);
      const sharedWithNext = this.coordsToNodes(this.openSector.nodes[i].next.x, this.openSector.nodes[i].next.z).filter(n => n.sector !== this.openSector);
      const newNeighbor = sharedWithNext.find(n1 => sharedWithCurr.some(n2 => n1.sector === n2.sector));
      if (newNeighbor) {
        this.openSector.nodes[i].neighboringSector = newNeighbor.sector;
        newNeighbor.sector.nodes.find(n => n.next.x == this.openSector.nodes[i].x &&
                                           n.next.z == this.openSector.nodes[i].z)
                                .neighboringSector = this.openSector;
      }
    }
  }

  // Return the previous node whose wall formed with the passed node is not between adjacent, already
  // existing sectors
  getOuterPrevNode(node) {
    const shared = this.coordsToNodes(node.x, node.z).filter(n => n !== node);
    if (shared.length == 0)
      return node.prev;
    const prevs = shared.filter(n => n.prev).map(n => n.prev);
    return prevs.find(prev => !shared.some(s => this.coordsToNodes(prev.x, prev.z)
                                     .filter(n => n !== prev)
                                     .some(n => s.sector === n.sector)));
  }

  splitSectorInTwo(splitSector, splittingSector) {
    // TODO: place/remove subsectors after splitting

    const head = splittingSector.nodes[splittingSector.nodes.length -1];
    const tail = splittingSector.nodes[0];
    // rotate splitSector so first element is the head of splitting sector
    while (splitSector.nodes[0].x != head.x && splitSector.nodes[0].z != head.z)
      splitSector.nodes.push(splitSector.nodes.shift());
    const tailIdx = splitSector.nodes.findIndex(n => n.x == tail.x && n.z == tail.z);
    const rightNodes = splitSector.nodes.slice(0, tailIdx);
    const leftNodes = splitSector.nodes.slice(tailIdx);

    let rightSplit = splittingSector.nodes.map(n => createNode(n.x, n.z));
    let leftSplit = [...rightSplit].reverse().map(n => createNode(n.x, n.z));
    rightSplit.pop();
    leftSplit.pop();
    rightSplit.forEach(n => n.neighboringSector = splitSector);
    leftSplit.forEach(n => n.neighboringSector = splittingSector);
    leftNodes.push(...leftSplit);
    rightNodes.push(...rightSplit);

    leftNodes.forEach((n, i) => {
      n.prev = leftNodes[i - 1];
      n.next = leftNodes[i + 1];
      n.sector = splitSector;
    });
    splitSector.nodes = leftNodes;
    splitSector.dirty = true;

    rightNodes.forEach((n, i) => {
      n.prev = rightNodes[i - 1];
      n.next = rightNodes[i + 1];
      n.sector = splittingSector;
    });
    splittingSector.nodes = rightNodes;
    splittingSector.floorTexNum = splitSector.floorTexNum;
    splittingSector.ceilTexNum = splitSector.ceilTexNum;
    splittingSector.floorHeight = splitSector.floorHeight;
    splittingSector.ceilHeight = splitSector.ceilHeight;
  }

  closeSector(openSector) {
    if (openSector.nodes.length < 2)
      return false;
    let tail = openSector.nodes[0];
    let head = openSector.nodes[openSector.nodes.length - 1];
    // Is the head connected to the tail?
    if (openSector.nodes.length > 3 &&
        head.x == tail.x &&
        head.z == tail.z) {
      openSector.nodes.pop();
      head = openSector.nodes[openSector.nodes.length - 1];
      for (let i = 0; i < openSector.nodes.length; i++) {
        openSector.nodes[i].prev = openSector.nodes[i - 1];
        openSector.nodes[i].next = openSector.nodes[i + 1];
      }
      return true;
    }

    let nodesSharedWithHead = this.coordsToNodes(head.x, head.z).filter(node => node !== head);
    let nodesSharedWithTail = this.coordsToNodes(tail.x, tail.z).filter(node => node !== tail);

    if (nodesSharedWithHead.length > 0 && nodesSharedWithTail.length > 0) {
      if (this.openSector.splitSector) {
        this.splitSectorInTwo(this.openSector.splitSector, this.openSector);
        return true;
      }

      let path1 = [this.getOuterLinkedNode(head, 'prev')];
      let path2 = [this.getOuterLinkedNode(tail, 'prev')];
      let isPath1Done = false;
      let isPath2Done = false;
      let path = null;
      let count = 0;
      while (true) {
        if (!isPath1Done) {
          if ( !(path1[path1.length - 1].x == tail.x && path1[path1.length - 1].z == tail.z) )
            path1.push(this.getOuterLinkedNode(path1[path1.length - 1], 'prev'));
          else {
            path1.pop();
            isPath1Done = true;
            if (this.areaOfPolygon(openSector.nodes.concat(path1)) > 0) {
              path = path1;
              break;
            }
          }
        }
        if (!isPath2Done) {
          if ( !(path2[path2.length - 1].x == head.x && path2[path2.length - 1].z == head.z) )
            path2.push(this.getOuterLinkedNode(path2[path2.length - 1], 'prev'));
          else {
            path2.pop();
            isPath2Done = true;
            if (this.areaOfPolygon([...openSector.nodes].reverse().concat(path2)) > 0) {
              path = path2;
              openSector.nodes.reverse();
              break;
            }
          }
        }
        count++;
        if (count > 100) {
          console.log('infinite loop');
          debugger;
          break;
        }
      }

      this.completeOpenSector(path);
      return true;
    }
    return false;
  }

  areaOfPolygon(points) {
    let sum = 0;
    for (let i = 0; i < points.length; i++) {
      const prev = i - 1 < 0 ? points[points.length - 1] : points[i - 1];
      const next = i + 1 > points.length - 1 ? points[0] : points[i + 1];
      sum += points[i].x * (next.z - prev.z);
    }
    return sum;
  }

  getOuterLinkedNode(node, linkDir /* 'next or 'prev' */) {
    const shared = this.coordsToNodes(node.x, node.z).filter(n => n !== node);
    if (shared.length == 0)
      return node[linkDir];
    const linked = shared.filter(n => n[linkDir]).map(n => n[linkDir]);
    // better name than s?
    return linked.find(linkedNode => !shared.some(s => this.coordsToNodes(linkedNode.x, linkedNode.z)
                                            .filter(n => n !== linkedNode)
                                            .some(n => s.sector === n.sector)));
  }

  drawGrid() {
    patternCtx.clearRect(0, 0, patternCan.width, patternCan.height);
    patternCtx.lineWidth = 0.5;
    patternCtx.beginPath();

    this.gridLineOffsetX = this.gridOffsetX % GRID_CELL_SIZE;
    if (this.gridLineOffsetX < 0)
      this.gridLineOffsetX = GRID_CELL_SIZE - Math.abs(this.gridLineOffsetX);

    this.gridLineOffsetZ = this.gridOffsetZ % GRID_CELL_SIZE;
    if (this.gridLineOffsetZ < 0)
      this.gridLineOffsetZ = GRID_CELL_SIZE - Math.abs(this.gridLineOffsetZ);

    // Vertical line
    patternCtx.moveTo(this.gridLineOffsetX, 0);
    patternCtx.lineTo(this.gridLineOffsetX, GRID_CELL_SIZE);

    // Horizontal line
    patternCtx.moveTo(0, this.gridLineOffsetZ);
    patternCtx.lineTo(GRID_CELL_SIZE, this.gridLineOffsetZ);

    patternCtx.stroke();

    const pattern = ctx.createPattern(patternCan, 'repeat');
    ctx.fillStyle = pattern;
    ctx.fillRect(0, 0, can.width, can.height);
  }

  drawCursor(x, z) {
    ctx.beginPath();
    ctx.arc(x, z, 5, 0, Math.PI*2);
    ctx.strokeStyle = 'maroon';
    ctx.lineWidth = 2;
    ctx.stroke();
  }

  update() {
    ctx.clearRect(0, 0, can.width, can.height);
    this.drawGrid();
    for (const sec of this.sectors)
      this.drawSector(sec);
    this.drawCursor(this.cursor.x, this.cursor.z);
  }

  drawPlayer(player) {
    ctx.beginPath();
    ctx.arc(player.x + this.gridOffsetX, player.z + this.gridOffsetZ, 5, 0, 2*Math.PI);
    ctx.fillStyle = 'red';
    ctx.fill();
    ctx.beginPath();
    ctx.moveTo(player.x + this.gridOffsetX, player.z + this.gridOffsetZ);
    ctx.lineTo(player.x + this.gridOffsetX + 10*Math.cos(player.angle), player.z + this.gridOffsetZ + 10*Math.sin(player.angle));
    ctx.lineWidth = 3;
    ctx.strokeStyle = 'red';
    ctx.stroke();
  }
}

export {SectorBuilder};
