import {SurfaceType} from './sector';
import {getSelectedTexNum} from './texture';
import {wraparoundArrayProxy} from './util';

export function setupRenderCanvas(canvas, containerDiv) {
  canvas.width = containerDiv.clientWidth;
  canvas.height = containerDiv.clientHeight;
}

export function setupReticleCanvas(canvas, containerDiv) {
  canvas.width = containerDiv.clientWidth;
  canvas.height = containerDiv.clientHeight;
}

export function drawReticle(ctx) {
  ctx.lineWidth = 1;
  ctx.strokeStyle = 'red';
  ctx.beginPath();
  ctx.moveTo(ctx.canvas.width/2, ctx.canvas.height/2 - 6);
  ctx.lineTo(ctx.canvas.width/2, ctx.canvas.height/2 + 6);
  ctx.closePath();
  ctx.stroke();
  ctx.beginPath();
  ctx.moveTo(ctx.canvas.width/2 - 6, ctx.canvas.height/2);
  ctx.lineTo(ctx.canvas.width/2 + 6, ctx.canvas.height/2);
  ctx.closePath();
  ctx.stroke();
}

export class Game {
  constructor(player) {
    this.player = player;
    this.sectors = wraparoundArrayProxy([]);
    this.curSector = null;
    this.selectedSurfaceList = [];
    this.focusedSurface = null;
  }

  getFocusedSurface(pixelData) {
    let type;
    switch (pixelData[3]) {
      case 0:
        type = SurfaceType.WALL;
        break;
      case 1:
        type = SurfaceType.LOWER_WALL;
        break;
      case 2:
        type = SurfaceType.UPPER_WALL;
        break;
      case 3:
        type = SurfaceType.FLOOR;
        break;
      case 4:
        type = SurfaceType.CEIL;
        break;
    }
    const sectorNum = pixelData[0];
    const wallNum = (type == SurfaceType.WALL ||
                     type == SurfaceType.LOWER_WALL ||
                     type == SurfaceType.UPPER_WALL) ? pixelData[1] : null;
    return {
      obj: wallNum != null ? this.sectors[sectorNum].nodes[wallNum] : this.sectors[sectorNum],
      type,
      sectorNum,
      wallNum
    };
  }

  // TODO: Set surface/node.selected where dirty sectors are updated
  surfaceSelectAction(surface, action) {
    if (surface.type == SurfaceType.FLOOR) {
      surface.obj.selected = (action == 'toggle') ? (surface.obj.selected | 0b001) : 0b000;
      surface.obj.dirty = true;
    } else if (surface.type == SurfaceType.CEIL) {
      surface.obj.selected = (action == 'toggle') ? (surface.obj.selected | 0b010) : 0b000;
      surface.obj.dirty = true;
    } else {
      if (surface.type == SurfaceType.WALL)
        surface.obj.selected = (action == 'toggle') ? (surface.obj.selected | 0b001) : 0b000;
      else if (surface.type == SurfaceType.LOWER_WALL)
        surface.obj.selected = (action == 'toggle') ? (surface.obj.selected | 0b010) : 0b000;
      else if (surface.type == SurfaceType.UPPER_WALL)
        surface.obj.selected = (action == 'toggle') ? (surface.obj.selected | 0b100) : 0b000;
      surface.obj.sector.dirty = true;
    }
  }

  unselectAllSurfaces() {
    for (const surface of this.selectedSurfaceList)
      this.surfaceSelectAction(surface, 'unselect');
    this.selectedSurfaceList = [];
  }

  // TODO: rename this - it doesn't really toggle
  toggleSurfaceSelection(surface, selectionType = 'single') {
    if (!document.pointerLockElement)
      return;

    if (selectionType == 'single' && this.selectedSurfaceList.length > 0)
      this.unselectAllSurfaces();

    let surfs = [];
    if (selectionType == 'single' || selectionType == 'multi')
      surfs.push(surface);
    else if (selectionType == 'adjacent') {
      const isFullWall = !this.sectors[surface.sectorNum].nodes[surface.wallNum].neighboringSector;
      let currIdx = surface.wallNum == 0 ? this.sectors[surface.sectorNum].nodes.length - 1: surface.wallNum - 1;
      let currNode = this.sectors[surface.sectorNum].nodes[currIdx];
      while (true) {
        if (currIdx == surface.wallNum)
          break;
        if (!currNode.neighboringSector != isFullWall) {
          currIdx = (currIdx == this.sectors[surface.sectorNum].nodes.length - 1) ? 0 : currIdx + 1;
          currNode = this.sectors[surface.sectorNum].nodes[currIdx];
          break;
        }
        currIdx = (currIdx == 0) ? this.sectors[surface.sectorNum].nodes.length - 1 : currIdx - 1;
        currNode = this.sectors[surface.sectorNum].nodes[currIdx];
      }
      for (let i = 0; i < this.sectors[surface.sectorNum].nodes.length && !currNode.neighboringSector == isFullWall; i++) {
        surfs.push({obj: currNode, type: SurfaceType.WALL, sectorNum: surface.sectorNum, wallNum: currIdx});
        currIdx = (currIdx == this.sectors[surface.sectorNum].nodes.length - 1) ? 0 : currIdx + 1;
        currNode = this.sectors[surface.sectorNum].nodes[currIdx];
      }
    }
    for (const surf of surfs) {
      this.surfaceSelectAction(surf, 'toggle');
      const i = this.selectedSurfaceList.findIndex(s => s.obj == surf.obj);
      if (i == -1)
        this.selectedSurfaceList.push(surf);
      else
        this.selectedSurfaceList.splice(i, 1);
    }
  }

  raiseFloorCeil(dir) {
    const focusedSurface = this.focusedSurface;
    if (focusedSurface && focusedSurface.type != SurfaceType.WALL) {
      const sector = this.sectors[focusedSurface.sectorNum];
      if (focusedSurface.type == SurfaceType.FLOOR)
        sector.floorHeight += dir;
      else if (focusedSurface.type == SurfaceType.CEIL)
        sector.ceilHeight += dir;
      sector.dirty = true;
    }
  }

  applyTexture() {
    for (const surface of this.selectedSurfaceList) {
      switch(surface.type) {
        case SurfaceType.FLOOR:
          surface.obj.floorTexNum = getSelectedTexNum();
          surface.obj.dirty = true;
          break;
        case SurfaceType.CEIL:
          surface.obj.ceilTexNum = getSelectedTexNum();
          surface.obj.dirty = true;
          break;
        case SurfaceType.WALL:
          surface.obj.texNum = getSelectedTexNum();
          surface.obj.sector.dirty = true;
          break;
        case SurfaceType.LOWER_WALL:
          surface.obj.lowerTexNum = getSelectedTexNum();
          surface.obj.sector.dirty = true;
          break;
        case SurfaceType.UPPER_WALL:
          surface.obj.upperTexNum = getSelectedTexNum();
          surface.obj.sector.dirty = true;
          break;
        default:
          console.error(`Failed to apply texture to unknown surface type '${surface.type}'.
            Valid types are ${Object.getOwnPropertyNames(SurfaceType).join(', ')}`);
      }
    }
  }
}
