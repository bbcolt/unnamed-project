import {vec2, vec3} from 'gl-matrix';

export function castRayToSurface(entity, sector, markedSectors = []) {
  if (!sector) return null;
  let farFront = vec3.create();
  vec3.add( farFront,
            entity.pos,
            vec3.scale( vec3.create(),
                        entity.front,
                        10000));
  let surface = null;
  let wallNum = 0;
  for (const [i, node] of sector.nodes.entries()) {
    if (!node.next)
      continue; // break instead?
    if (node.neighboringSector && markedSectors.includes(node.neighboringSector))
      continue;
    else {
      let hit = intersect(entity.x, entity.z, farFront[0], farFront[2], node.x, node.z, node.next.x, node.next.z);
      if (hit) {
        if (!surface) {
          surface = hit;
          surface.node = node;
          surface.nodeIndex = wallNum;
        }
        else if (hit.t < surface.t) {
          surface = hit;
          surface.node = node;
          surface.nodeIndex = wallNum;
        }
      } else {
        if (!node.neighboringSector)
          wallNum++;
        else {
          if (sector.ceilHeight > node.neighboringSector.ceilHeight)
            wallNum++;
          if (sector.floorHeight < node.neighboringSector.floorHeight)
            wallNum++;
        }
      }
    }
  }
  if (surface) {
    surface.sector = sector;
    let a = Math.sqrt(Math.pow(surface.x - entity.x, 2) + Math.pow(surface.y - entity.z, 2));
    let topO = sector.ceilHeight - entity.y;
    let topAngle = Math.atan(topO/a);
    let bottomO = entity.y - sector.floorHeight;
    let bottomAngle = -Math.atan(bottomO/a);
    if (entity.pitch > topAngle)
      surface.type = 'ceil';
    else if (entity.pitch < bottomAngle)
      surface.type = 'floor';
    else {
      if (surface.node.neighboringSector) {
        // upper portal wall
        if (sector.ceilHeight > surface.node.neighboringSector.ceilHeight) {
          let topBottomO = surface.node.neighboringSector.ceilHeight - entity.y
          let topBottomAngle = Math.atan(topBottomO/a);
          if (entity.pitch > topBottomAngle) {
            surface.type = 'wall';
            return surface;
          }
          surface.nodeIndex++;
        }
        // lower portal wall
        if (sector.floorHeight < surface.node.neighboringSector.floorHeight) {
          let bottomTopO = entity.y - surface.node.neighboringSector.floorHeight;
          let bottomTopAngle = -Math.atan(bottomTopO/a);
          if (entity.pitch < bottomTopAngle) {
            surface.type = 'wall';
            return surface;
          }
          surface.nodeIndex++;
        }
        markedSectors.push(sector);
        return castRayToSurface(entity, surface.node.neighboringSector, markedSectors);
      }
      sector.nodeIndex++;
      surface.type = 'wall';
    }
    return surface;
  } else {
    return null;
  }
}

export function intersect(x1, y1, x2, y2, x3, y3, x4, y4) {
  // t = (q - p) x s / (r x s)
  // s = (p - q) x r / (r x s)
  let p = vec2.fromValues(x1, y1);
  let q = vec2.fromValues(x3, y3);
  let r = vec2.sub(vec2.create(), vec2.fromValues(x2, y2), p);
  let s = vec2.sub(vec2.create(), vec2.fromValues(x4, y4), q);

  let b = perpdot(r, s);
  let a = vec2.sub(vec2.create(), q, p);
  let t = perpdot(a, s) / b;

  let u = perpdot(a, r) / b;

  if (t >= 0 && t <= 1 && u >= 0 && u <= 1) {
    let intersection = vec2.add(vec2.create(), p, vec2.scale(vec2.create(), r, t));
    return {x: intersection[0], y: intersection[1], t};
  } else {
    return null;
  }
}

function perpdot(v, w) {
  return v[0]*w[1] - v[1]*w[0];
}
