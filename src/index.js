import {Renderer} from './renderer';
import Controller from './controller';
import {SectorBuilder} from './sectorBuilder';
import {InputManager, InputType, MOD_KEY_1, MOD_KEY_2, MOD_KEY_3} from './input';
import Camera from './camera';
import {Game, getFocusedSurface, setupRenderCanvas, setupReticleCanvas, drawReticle} from './game';
import {Player} from './player';
import {vec3} from 'gl-matrix';
import {onImageUpload} from './texture';
import {setupTexture} from './glHelpers';
import {curSector} from './sector';
import {castRayToSurface} from './raycast';
import {getFocusedSurfaceDebugString} from './debug';
import {MoveForwardCommand, MoveRightCommand, RotateCommand, MoveUpCommand, RaiseFloorCeilCommand,
        SetTextureCommand, ToggleSurfaceSelectionCommand, DropNodeCommand, DebugCommand,
        RequestPointerLockCommand, argMixin, mouseMovementXZArgMixin, scrollDeltaYArgMixin,
        modKeysArgMixin, SetIsPanningCommand, GrabCommand, MoveCursorCommand,
        mouseMovementXZOffsetXZArgMixin, UnselectAllSurfacesCommand} from './command';

const renderCanvasContainerDiv = document.querySelector('#container-canvas-render');
const renderCanvas = document.querySelector('#canvas');
setupRenderCanvas(renderCanvas, renderCanvasContainerDiv);
const reticleCanvas = document.querySelector('#canvas-reticle');
setupReticleCanvas(reticleCanvas, renderCanvasContainerDiv);
const reticleCtx = reticleCanvas.getContext('2d', {alpha: true});
drawReticle(reticleCtx);

const gl = renderCanvas.getContext('webgl2', { alpha: true,
                                               /*
                                               desynchronized: true,
                                               atialias: true,
                                               depth: true,
                                               powerPreference: 'high-performance',
                                               premultipliedAlpha: true,
                                               preserveDrawingBuffer: true */ });
if (!gl)
  alert('Your browser does not support webgl2');

const tex = gl.createTexture();
setupTexture(gl, tex);
const texAtlasCanvas = document.querySelector('#canvas-texture-atlas');
const texAtlasCtx = texAtlasCanvas.getContext('2d', {alpha: false});
document.querySelector('#input-texture-upload').addEventListener('change', onImageUpload(gl, texAtlasCtx, tex));

const mapCanvas = document.querySelector('#canvas-map');

const player = new Player();
const game = new Game(player);
const renderer = new Renderer(gl, player.camera, game);
const sb = new SectorBuilder(game.sectors);

const im = new InputManager();
im.registerCommand(new MoveForwardCommand(player),  { input: InputType.KEY_W,
                                                      argMixin: argMixin({dir: 1}),
                                                      repeat: true });

im.registerCommand(new MoveForwardCommand(player),  { input: InputType.KEY_S,
                                                      argMixin: argMixin({dir: -1}),
                                                      repeat: true });

im.registerCommand(new MoveRightCommand(player),    { input: InputType.KEY_A,
                                                      argMixin: argMixin({dir: -1}),
                                                      repeat: true });

im.registerCommand(new MoveRightCommand(player),    { input: InputType.KEY_D,
                                                      argMixin: argMixin({dir: 1}),
                                                      repeat: true });

im.registerCommand(new RotateCommand(player),       { input: InputType.KEY_ARROWUP,
                                                      argMixin: argMixin({x: 0, z: 1}),
                                                      repeat: true });

im.registerCommand(new RotateCommand(player),       { input: InputType.KEY_ARROWDOWN,
                                                      argMixin: argMixin({x: 0, z: -1}),
                                                      repeat: true });

im.registerCommand(new RotateCommand(player),       { input: InputType.KEY_ARROWLEFT,
                                                      argMixin: argMixin({x: 1, z: 0}),
                                                      repeat: true });

im.registerCommand(new RotateCommand(player),       { input: InputType.KEY_ARROWRIGHT,
                                                      argMixin: argMixin({x: -1, z: 0}),
                                                      repeat: true });

im.registerCommand(new RotateCommand(player),       { input: InputType.MOUSE_MOVE,
                                                      pointerLockOnly: true,
                                                      argMixin: mouseMovementXZArgMixin(-1/20, -1/30) });

im.registerCommand(new MoveUpCommand(player),       { input: InputType.KEY_E,
                                                      argMixin: argMixin({dir: 1}),
                                                      repeat: true });

im.registerCommand(new MoveUpCommand(player),       { input: InputType.KEY_Q,
                                                      argMixin: argMixin({dir: -1}),
                                                      repeat: true });

im.registerCommand(new RaiseFloorCeilCommand(game), { input: InputType.MOUSE_SCROLL,
                                                      argMixin: scrollDeltaYArgMixin(3),
                                                      pointerLockOnly: true,
                                                      repeat: false });

im.registerCommand(new SetTextureCommand(game),     { input: InputType.KEY_T });

im.registerCommand(new UnselectAllSurfacesCommand(game),     { input: InputType.KEY_BACKSPACE });

im.registerCommand(new ToggleSurfaceSelectionCommand(game),  { input: InputType.MOUSE_LEFT,
                                                               argMixin: modKeysArgMixin() });

im.registerCommand(new DropNodeCommand(sb),         { input: InputType.KEY_SPACE });

im.registerCommand(new RequestPointerLockCommand(reticleCanvas), { input: InputType.MOUSE_LEFT,
                                                                   target: reticleCanvas });

im.registerCommand(new SetIsPanningCommand(sb, true),  { input: InputType.MOUSE_RIGHT,
                                                         target: mapCanvas });

im.registerCommand(new SetIsPanningCommand(sb, false), { input: InputType.MOUSE_RIGHT,
                                                         target: mapCanvas,
                                                         onRelease: true });

im.registerCommand(new GrabCommand(sb, true),       { input: InputType.MOUSE_LEFT,
                                                      target: mapCanvas });

im.registerCommand(new GrabCommand(sb, false),      { input: InputType.MOUSE_LEFT,
                                                      target: mapCanvas,
                                                      onRelease: true });

im.registerCommand(new MoveCursorCommand(sb),       { input: InputType.MOUSE_MOVE,
                                                      target: mapCanvas,
                                                      argMixin: mouseMovementXZOffsetXZArgMixin(mapCanvas.getBoundingClientRect()) });

im.registerCommand(new DebugCommand(sb),            { input: InputType.KEY_Y });

(function loop() {
  im.processInput();

  player.curSector = curSector(player.curSector, game.sectors, player.x, player.z);

  sb.update();
  sb.drawPlayer(player);

  renderer.render();

  const pixel = renderer.getPixelUnderReticle();
  game.focusedSurface = game.getFocusedSurface(pixel);
  document.querySelector('#wall-num').innerHTML = getFocusedSurfaceDebugString(game.focusedSurface);

  requestAnimationFrame(loop);
})()


