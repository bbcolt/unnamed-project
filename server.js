const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
require('dotenv').config();

const app = express();
const port = 3000;

app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

app.get('/maps/:mapId', (req, res) => {
    const connection = mysql.createConnection({
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASS,
        database: process.env.DB_NAME
    });
    connection.connect();
    connection.query(`SELECT sectors FROM ${process.env.DB_TABLE_MAP} WHERE id=? LIMIT 1`, [req.params.mapId], (error, results, fields) => {
        if (error) throw error;
        res.send(results[0]);
    });
    connection.end();
});

app.post('/', (req, res) => {
    const connection = mysql.createConnection({
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASS,
        database: process.env.DB_NAME
    });
    connection.connect();
    const mapData = JSON.stringify(req.body);
    connection.query('INSERT INTO map (sectors) VALUES (?)', [mapData], (error, results, fields) => {
        if (error) throw error;
        console.log('POST successful');
    });
    connection.end();

    res.set('Content-Type', 'text/html');
    res.send('Got a POST request')
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
